﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace tpHotel
{
    public partial class ajouterChambre : Form
    {
        private string etage;
        private string description;
        private string hotel;

        public ajouterChambre()
        {
            InitializeComponent();

        }

        private void btn_fermer_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_annuler_Click(object sender, EventArgs e)
        {
            num_etage.ResetText();
            txt_description.ResetText();
            combo_hotel.ResetText();
        }

        private void btn_enregistrer_Click(object sender, EventArgs e)
        {
            foreach (Hotel item in Persistance.getLesHotels()) 
            {
                if (item.Nom == combo_hotel.Text)
                {
                    Persistance.ajouterChambre((int)num_etage.Value, txt_description.Text, item.Id);
                }
            }

            MessageBox.Show("le numéro d'étage est : " + num_etage.Value + "la description est : " + txt_description.Text + "dans l'hotel : " + combo_hotel.Text);
        }

        private void ajouterChambre_Load(object sender, EventArgs e)
        {
            foreach (Hotel Item in Persistance.getLesHotels())
            {
                combo_hotel.Items.Add(Item.Nom);

            }

        }

    }
}

