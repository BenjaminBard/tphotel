﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace tpHotel
{
    public partial class VoirLesChambres : Form
    {
        public VoirLesChambres()
        {
            InitializeComponent();
        }

        private void VoirLesChambres_Load(object sender, EventArgs e)
        {
            lst_chambres.DataSource = Persistance.getLesChambres();
        }

        private void btn_fermer_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
