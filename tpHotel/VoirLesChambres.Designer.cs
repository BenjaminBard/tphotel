﻿namespace tpHotel
{
    partial class VoirLesChambres
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lst_chambres = new System.Windows.Forms.DataGridView();
            this.btn_fermer = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.lst_chambres)).BeginInit();
            this.SuspendLayout();
            // 
            // lst_chambres
            // 
            this.lst_chambres.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.lst_chambres.Location = new System.Drawing.Point(48, 33);
            this.lst_chambres.Name = "lst_chambres";
            this.lst_chambres.Size = new System.Drawing.Size(485, 301);
            this.lst_chambres.TabIndex = 3;
            // 
            // btn_fermer
            // 
            this.btn_fermer.Location = new System.Drawing.Point(494, 368);
            this.btn_fermer.Name = "btn_fermer";
            this.btn_fermer.Size = new System.Drawing.Size(86, 39);
            this.btn_fermer.TabIndex = 4;
            this.btn_fermer.Text = "Fermer";
            this.btn_fermer.UseVisualStyleBackColor = true;
            this.btn_fermer.Click += new System.EventHandler(this.btn_fermer_Click);
            // 
            // VoirLesChambres
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(648, 438);
            this.Controls.Add(this.btn_fermer);
            this.Controls.Add(this.lst_chambres);
            this.Name = "VoirLesChambres";
            this.Text = "VoirLesChambres";
            this.Load += new System.EventHandler(this.VoirLesChambres_Load);
            ((System.ComponentModel.ISupportInitialize)(this.lst_chambres)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView lst_chambres;
        private System.Windows.Forms.Button btn_fermer;
    }
}